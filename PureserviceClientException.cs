using System;
using System.Runtime.Serialization;

namespace PureserviceLibrary
{
    [Serializable]
    internal class PureserviceClientException : Exception
    {
        public PureserviceClientException()
        {
        }

        public PureserviceClientException(string message) : base(message)
        {
        }

        public PureserviceClientException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected PureserviceClientException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}