﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PureserviceLibrary
{
    public class PureserviceClient
    {
        private string _baseUrl;
        private string _apiKey;

        public PureserviceClient(string baseUrl,string apiKey){
            _apiKey = apiKey;
            _baseUrl = baseUrl.TrimEnd('/')+"/";
        }

        public string CreateTask(){
            return Post("task",Models.Task.PostTaskJson("test","test2",2)).Result;
        }

        private async Task<string> Get(string path)
        {
            int counter = 0;
            bool successfull = false;

            while(!successfull && counter < 5)
            {
                counter++;
                using (var client = new HttpClient())
                {
                    var pureserviceUrl = _baseUrl + "api/" + path.Trim('/');
                    client.DefaultRequestHeaders.TryAddWithoutValidation("X-Authorization-Key", _apiKey);
                    client.DefaultRequestHeaders.Add("Accept", "application/vnd.api+json");

                    #if DEBUG
                        Console.WriteLine("GET to: " + pureserviceUrl);
                    #endif

                    using (var request = await client.GetAsync(new Uri(pureserviceUrl)))
                    {
                        if (!request.IsSuccessStatusCode)
                        {
                            continue;
                        }

                        var content = await request.Content.ReadAsStringAsync();
                        successfull = true;
                        return content;
                    }
                }
            }

            throw new PureserviceClientException("Pureservice did not respons properly, was unable to get the requested data");
        }

        private async Task<string> Post(string path, string body)
        {
            int counter = 0;
            bool successfull = false;
            while(!successfull && counter < 5)
            {
                counter++;
                using (var client = new HttpClient())
                {
                    var pureserviceUrl = _baseUrl + "api/" + path;
                    client.DefaultRequestHeaders.TryAddWithoutValidation("X-Authorization-Key", _apiKey);
                    client.DefaultRequestHeaders.Add("Accept", "application/vnd.api+json");

                    #if DEBUG
                        Console.WriteLine("POST to: " + pureserviceUrl);
                    #endif

                    var c = new StringContent(body,Encoding.UTF8,"application/vnd.api+json");

                    using (var request = await client.PostAsync(new Uri(pureserviceUrl),c))
                    {
                        if (!request.IsSuccessStatusCode)
                        {
                            continue;
                        }

                        var content = await request.Content.ReadAsStringAsync();
                        successfull = true;
                        return content;
                    }
                }
            }

            throw new PureserviceClientException("Pureservice did not respons properly, was unable to get the requested data");
        }
    }
}
