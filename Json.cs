using System.IO;
using Newtonsoft.Json;

namespace PureserviceLibrary
{
    public static class Json
    {
        public static string Convert(object o){
            var writer = new StringWriter();
			var serializer = new JsonSerializer();
			serializer.Serialize(writer, o);
			return writer.GetStringBuilder().ToString();
        }
    }
}