﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace PureserviceLibrary.Models
{
	public class CompanyList
	{
		[JsonProperty("companies")]
		public List<Company> Companies { get; set; }
	}
}
