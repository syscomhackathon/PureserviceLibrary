﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace PureserviceLibrary.Models
{
	public class LinkedTicketList
	{
		[JsonProperty("tickets")]
		public List<Ticket> Tickets { get; set; }

		[JsonProperty("linked")]
		public Linked Linked { get; set; }
	}
}
