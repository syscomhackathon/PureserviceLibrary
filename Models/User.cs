﻿using Newtonsoft.Json;
using PureserviceLibrary.Models;
using System;
using System.Linq;

namespace PureserviceLibrary.Models
{
	public class User
	{
		[JsonProperty("id")]
		public long Id { get; set; }

		[JsonProperty("firstName")]
		public string FirstName { get; set; }

		[JsonProperty("lastName")]
		public string LastName { get; set; }

		[JsonProperty("middleName")]
		public string MiddleName { get; set; }

		[JsonProperty("title")]
		public string Title { get; set; }

		[JsonProperty("companyId")]
		public long CompanyId { get; set; }

        [JsonProperty("emailAddressId")]
        public long EmailAddressId { get; set; }

        public EmailAddressList EmailAddressList { get; set; }

        private TicketList _ticketList = new TicketList();

		public TicketList TicketList { get { return _ticketList; } set { _ticketList = value; } }

        public int NumberOfTicketsPreviousTwelveMonths()
        {
            return TicketList.Tickets.Count(t => t.Created > DateTime.Now.AddMonths(-12));
        }

        public Ticket LatestRegisteredTicket()
        {
            return TicketList.Tickets.OrderByDescending(t => t.Created).First();
        }
    }
}
