﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace PureserviceLibrary.Models
{
	public class UserList
	{
		private List<User> _users = new List<User>();

		[JsonProperty("users")]
		public List<User> Users { get { return _users; } set { _users = value; } }

	}
}
