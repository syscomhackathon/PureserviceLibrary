﻿using Newtonsoft.Json;

namespace PureserviceLibrary.Models
{
	public class Priority
	{
		[JsonProperty("id")]
		public int Id { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("requestTypeId")]
		public int RequestTypeId { get; set; }
	}
}
