﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PureserviceLibrary.Models
{
    public class EmailAddress
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("userId")]
        public string UserId { get; set; }
    }
}
