﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace PureserviceLibrary.Models
{
	public class Linked
	{
		[JsonProperty("priorities")]
		public List<Priority> Priorities { get; set; } = new List<Priority>();

		[JsonProperty("users")]
		public List<User> Users { get; set; } = new List<User>();
	}
}
