﻿using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;

namespace PureserviceLibrary.Models
{
	public class Company
	{
		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("website")]
		public string Website { get; set; }

		[JsonProperty("id")]
		public long Id { get; set; }

		[JsonProperty("companyNumber")]
		public string CustomerNumber { get; set; }

		[JsonProperty("usersCount")]
		public long UsersCount { get; set; }

		private UserList _userList = new UserList();

		public UserList UserList { get { return _userList; } set { _userList = value; } }

        public IEnumerable<User> TopThreeUsersPreviousTwelveMonths()
        {
            return UserList.Users.OrderByDescending(u => u.NumberOfTicketsPreviousTwelveMonths()).Take(2).Where(ut => ut.TicketList.Tickets.Count > 0);
        }

        public IEnumerable<User> TopThreeCompanyUsers()
        {
            return UserList.Users.OrderByDescending(u => u.TicketList.Tickets.Count).Take(2).Where(ut => ut.TicketList.Tickets.Count > 0);
        }
    }
}
