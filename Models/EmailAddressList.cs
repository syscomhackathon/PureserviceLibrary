﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PureserviceLibrary.Models
{
    public class EmailAddressList
    {
        private List<EmailAddress> _emailAddress = new List<EmailAddress>();

        [JsonProperty("emailaddresses")]
        public List<EmailAddress> EmailAddresses { get { return _emailAddress; } set { _emailAddress = value; } }
    }
}
