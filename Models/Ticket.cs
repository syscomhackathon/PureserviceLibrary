﻿using System;
using Newtonsoft.Json;

namespace PureserviceLibrary.Models
{
	public class Ticket
	{
		[JsonProperty("id")]
		public long Id { get; set; }

		[JsonProperty("requestNumber")]
		public long RequestNumber { get; set; }

		[JsonProperty("subject")]
		public string Subject { get; set; }

		[JsonProperty("description")]
		public string Description { get; set; }

		[JsonProperty("priorityId")]
		public int PriorityId { get; set; }

		[JsonProperty("statusId")]
		public int StatusId { get; set; }

		[JsonProperty("userId")]
		public long UserId { get; set; }

		[JsonProperty("created")]
		public DateTime Created { get; set; }
	}
}
