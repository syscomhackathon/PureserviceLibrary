﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace PureserviceLibrary.Models
{
	public class TicketList
	{
		private List<Ticket> _tickets = new List<Ticket>();

		[JsonProperty("tickets")]
		public List<Ticket> Tickets { get { return _tickets; } set { _tickets = value; } }
	}
}
