﻿using Newtonsoft.Json;

namespace PureserviceLibrary.Models
{
	public class Status
	{
		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("id")]
		public int Id { get; set; }

		[JsonProperty("userDisplayName")]
		public string UserDisplayName { get; set; }

		[JsonProperty("coreStatus")]
		public int CoreStatus { get; set; }
	}
}
