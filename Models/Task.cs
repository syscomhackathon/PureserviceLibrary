using System;
using System.IO;
using Newtonsoft.Json;

namespace PureserviceLibrary.Models
{
	public class Task
	{
		[JsonProperty("id")]
		public int? Id { get; set; }

		[JsonProperty("summary")]
		public string Summary { get; set; }

		[JsonProperty("details")]
		public string Details { get; set; }

		[JsonProperty("status")]
		public int Status { get; set; }

		[JsonProperty("index")]
		public int Index { get; set; }

		[JsonProperty("created")]
		public DateTime? Created { get; set; }

		[JsonProperty("modified")]
		public DateTime? Modified { get; set; }

		public static string PostTaskJson(string summary,string details, int ticketId,
			int? agentId = null, int? teamId = null, int status = 0, int index = 1) {
				
			return Json.Convert(new {
				tasks = new [] {
					new {
						summary = summary,
						details = details,
						index = index,
						status = status,
						links = new {
							assignedAgent = new {
								id = agentId
							},
							assignedTeam = new {
								id = teamId
							},
							ticket = new {
								id = ticketId
							}
						}
					}
				}
			});
			
		}
	}
}
