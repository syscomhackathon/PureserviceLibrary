﻿using Newtonsoft.Json;

namespace PureserviceLibrary.Models
{
	public class Team
	{
		[JsonProperty("id")]
		public int Id { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("departmentId")]
		public int DepartmentId { get; set; }

		[JsonProperty("email")]
		public string Email { get; set; }
	}
}
