﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace PureserviceLibrary.Models
{
	public class TeamList
	{
		[JsonProperty("teams")]
		public List<Team> Teams { get; set; }
	}
}
