﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace PureserviceLibrary.Models
{
	public class StatusList
	{
		[JsonProperty("statuses")]
		public List<Status> Statuses { get; set; }
	}
}
