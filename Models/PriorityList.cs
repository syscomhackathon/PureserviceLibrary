﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace PureserviceLibrary.Models
{
	public class PriorityList
	{
		[JsonProperty("priorities")]
		public List<Priority> Priorities { get; set; }
	}
}
